<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Locale;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature;

use Zend\I18n\Translator\Translator;
use Zend\Validator\AbstractValidator;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        //$e->getApplication()->getServiceManager()->get('translator');
//        $translator = $e->getApplication()->getServiceManager()->get('translator');
//        $translator
//            ->setLocale(\Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']))
//            ->setFallbackLocale('en_US');


        // Визначення Локалі, і конфігурація Zend\Translate відповідно
        $default   = 'ru';
        $supported = array('ru_RU','ru_RU', 'ru', 'en-US', 'en-GB', 'en', 'nl-NL', 'nl');
        $app       = $e->getApplication();
        $headers   = $app->getRequest()->getHeaders();

        $translator = $e->getApplication()->getServiceManager()->get('translator');

        $match_locale = $default;
        if ($headers->has('Accept-Language')) {
            $locales = $headers->get('Accept-Language')->getPrioritized();

            // Loop through all locales, highest priority first
            foreach ($locales as $locale) {
                if (!!($match = Locale::lookup($supported, $locale->getLanguage()))) {
                    // The locale is one of our supported list
                    if (strlen($match) <=2) {
                        $match = $match . '_'. strtoupper($match);
                    }
                    $translator->setLocale($match);
                    Locale::setDefault($match);

                    $match_locale = $match;
                    break;
                }
            }

            if (!$match) {
                // Nothing from the supported list is a match
                $translator->setLocale($default);
                Locale::setDefault($default);

            }
        } else {
            $translator->setLocale($default);
            Locale::setDefault($default);
        }

        $translator->setFallbackLocale($default);

        /*
         * Для повідомлень валідацій форм і тд, потрібно окремо встановлювати
         * \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
         * \Zend\Validator\AbstractValidator::setDefaultTranslatorTextDomain();
         */

        $loc = $match_locale;
        if (strlen($match_locale) > 2) {
            $loc = strtolower(substr($match_locale, 0, 2));
        }
        $translator->addTranslationFile(
            'phpArray',
            './module/Application/language/validator/'. $loc .'/Zend_Validate.php'
//            ,
//            false,
//            $match_locale
        );

        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        //\ZfcUser\Validator\AbstractRecord::setDefaultTranslator($translator);

        $app->getServiceManager()->get('ViewHelperManager')->get('translate')
            ->setTranslator($translator);

        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
