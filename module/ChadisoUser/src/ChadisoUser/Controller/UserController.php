<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ChadisoUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ChadisoUser\Controller;

//use Zend\Mvc\Controller\AbstractActionController;

use ZfcUser\Controller\UserController as ZfcUserController;

use Zend\EventManager\EventManagerInterface as EventManagerInterface;

class UserController extends ZfcUserController
{
    //protected $failedLoginMessage = 'Authentication failed. Please try again.';


    public static function dumpClassName($className) {
        \Zend\Debug\Debug::dump(get_class($className));
    }

    public static function dumpClassMethods($className) {
        \Zend\Debug\Debug::dump(get_class_methods($className));
    }


    public function setEventManager(EventManagerInterface $events) {

        $events->attach('dispatch', array($this, 'preDispatch'), 100);

        parent::setEventManager($events);
    }


    public function preDispatch() {
//        var_dump('This is Sparta! Opps - preDispatch!');

//        $translator = $e->getApplication()->getServiceManager()->get('translator');
//
//        $app->getServiceManager()->get('ViewHelperManager')->get('translate')
//            ->setTranslator($translator);


        //оце ніби працює
        $translator = $this->getServiceLocator()->get('translator');

        $viewHelper = $this->getServiceLocator()->get('ViewHelperManager');

        $viewHelper->get('translate')->setTranslator($translator);

        $this->setFailedLoginMessage($translator->translate($this->getFailedLoginMessage()));


//        var_dump(get_class($helper = $this->getServiceLocator()->get('viewmanager')->getRenderer()->plugin('flashMessenger')));
//
//        $helper->setTranslator($translator);
//        var_dump($helper->getTranslatorTextDomain());

        //var_dump($this->flashMessenger()->setNamespace('zfcuser-login-form')->getMessages());

        //\Zend\Validator\AbstractValidator::setDefaultTranslator($translator);



    }

    public function setFailedLoginMessage($message = null) {
        $this->failedLoginMessage = $message;
    }

    public function getFailedLoginMessage() {
        return $this->failedLoginMessage;
    }

    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /module-specific-root/skeleton/foo
        return array();
    }

    public function gridaliciousAction() {
        \Zend\Debug\Debug::dump('gridalicious plugin example');
        return array();
    }

    public function masonryAction() {
        //\Zend\Debug\Debug::dump('masonry plugin example');
        $headScript = $this->getServiceLocator()->get('viewhelpermanager')->get('HeadScript');

        //$this->dumpClassMethods($headScript);
        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');

        $headScript
        ->appendFile($renderer->basePath() . '/js/box-maker.js')
        ->appendFile($this->getRequest()->getBasePath(). '/js/jquery.masonry.js')
        ->appendFile($this->getRequest()->getBasePath() . '/js/modernizr-transitions.js')
        ->appendFile($this->getRequest()->getBasePath() . '/js/jquery.infinitescroll.min.js')
        ;
        return array();
    }


    public function freetileAction() {

        $headScript = $this->getServiceLocator()->get('viewhelpermanager')->get('HeadScript');

        //$this->dumpClassMethods($headScript);
        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');

        $headScript
            ->appendFile($this->getRequest()->getBasePath(). '/js/jquery.freetile.js')
            ->appendFile($this->getRequest()->getBasePath() . '/js/modernizr-transitions.js')
            ->appendFile($this->getRequest()->getBasePath() . '/js/jquery.infinitescroll.min.js')
        ;
        return array();
    }


}