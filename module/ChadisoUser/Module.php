<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ChadisoUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ChadisoUser;

use Zend\ModuleManager\Feature\BootstrapListenerInterface;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use \Zend\EventManager\EventInterface;

class Module implements AutoloaderProviderInterface, BootstrapListenerInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(EventInterface $e)
    {

        //ZfcUser\Controller\UserController
        $e->getApplication()->getEventManager()->getSharedManager()->attach('ChadisoUser\Controller\UserController', 'dispatch', function($e) {

            // змінюєм для конкрентного екшена лайаут
            //@todo перевірка на контроллер

            $controller = $e->getTarget();

//            $message = $controller->getServiceLocator()->get('ViewHelperManager')->get('translator')->translate($controller->getFailedLoginMessage());
//            $controller->setFailedLoginMessage($message);

            switch($e->getRouteMatch()->getParam('action')) {
                case 'index':
//                    var_dump($controller->layout()->getTemplate());
                    $controller->layout('zfc-user/layout/profile');
//                    var_dump($controller->layout()->getTemplate());
                    break;
            }

        });

        ;

        $events = $e->getApplication()->getEventManager()->getSharedManager();
        $events->attach('ZfcUser\Form\Login','init', function($e) {
            $form = $e->getTarget();
            // Змінюєм елементи ZfcUser форми, як нам треба
            // Це для Poedit
            $form->get('identity')->setLabel('email');
            $form->get('credential')->setLabel('password');
            $form->get('submit')->setLabel('Sign In');

            //$form->get('identity')->setMessages();
//            $form->add(array(
//                'type' => 'Zend\Form\Element\Select',
//                'options' => array(
//                    'label' => 'Which is your mother tongue?',
//                    'value_options' => array(
//                        '0' => 'French',
//                        '1' => 'English',
//                        '2' => 'Japanese',
//                        '3' => 'Chinese',
//                    ),
//                ),/
//                'name' => 'language'
//            ));


        });

        $events->attach('ZfcUser\Form\Register','init', function($e) {
            $form = $e->getTarget();
            // Змінюєм елементи ZfcUser форми, як нам треба
            // Це для Poedit

            $form->get('password')->setLabel('Password');
            $form->get('passwordVerify')->setLabel('Password verify');
            $form->get('submit')->setLabel('Register');

        });


        // You may not need to do this if you're doing it elsewhere in your
        // application
//        $eventManager        = $e->getApplication()->getEventManager();
//        $moduleRouteListener = new ModuleRouteListener();
//        $moduleRouteListener->attach($eventManager);


    }


}
