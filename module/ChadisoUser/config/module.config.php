<?php

namespace ChadisoUser;

return array(
    'controllers' => array(
        'invokables' => array(
            //'ChadisoUser\Controller\User' => 'ChadisoUser\Controller\UserController',
            'chadisouser' => 'ChadisoUser\Controller\UserController',
            'zfcuser' => 'ChadisoUser\Controller\UserController'
        ),
    ),

    // edp-module-layouts module config
    'module_layouts' => array(
        //overriding layout for zfcuser module output
        'ChadisoUser' => 'zfc-user/layout/layout', //chadiso-user/layout/layout',
        'ZfcUser' => 'zfc-user/layout/layout'
    ),
    'router' => array(
        'routes' => array(
            //overriding zfcuser module's routes
            'zfcuser' => array(
                'type' => 'Literal',
                'priority' => 1000,
                'options' => array(
                    'defaults' => array(
                        'controller' => 'chadisouser', //'ChadisoUser\Controller\User',
                    ),
                    'route' => '/account',
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'login' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/login',
                            'defaults' => array(
                                'controller' => 'chadisouser',
                                'action'     => 'login',
                            ),
                        ),
                    ),
                    'authenticate' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/authenticate',
                            'defaults' => array(
                                'controller' => 'chadisouser',
                                'action'     => 'authenticate',
                            ),
                        ),
                    ),
                    'logout' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/logout',
                            'defaults' => array(
                                'controller' => 'chadisouser',
                                'action'     => 'logout',
                            ),
                        ),
                    ),
                    'register' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/register',
                            'defaults' => array(
                                'controller' => 'chadisouser',
                                'action'     => 'register',
                            ),
                        ),
                    ),
                    'changepassword' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/change-password',
                            'defaults' => array(
                                'controller' => 'chadisouser',
                                'action'     => 'changepassword',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'query' => array(
                                'type' => 'Query',
                            ),
                        ),
                    ),
                    'changeemail' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/change-email',
                            'defaults' => array(
                                'controller' => 'chadisouser',
                                'action' => 'changeemail',
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'query' => array(
                                'type' => 'Query',
                            ),
                        ),
                    )
                )

            ),

            'chadisouser' => array(
                'type'    => 'Literal',
                'options' => array(
                    'defaults' => array(
                        'controller' => 'chadisouser',
                    ),
                    'route' => '/chmooded',
//                    // Change this to something specific to your module
//                    'route'    => '/chmooded-user',
//                    'defaults' => array(
//                        // Change this value to reflect the namespace in which
//                        // the controllers for your module are found
//                        '__NAMESPACE__' => 'ChadisoUser\Controller',
//                        'controller'    => 'User',
//                        'action'        => 'index',
//                    ),

                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                    'foo' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/foo',
                            'defaults' => array(
                                'controller' => 'user',
                                'action'     => 'foo',
                            ),
                        ),
                    ),
                    'gridalicious' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/gridalicious',
                            'defaults' => array(
                                'controller' => 'chadisouser',
                                'action'     => 'gridalicious',
                            ),
                        ),
                    ),
                    'masonry' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/masonry',
                            'defaults' => array(
                                'controller' => 'chadisouser',
                                'action'     => 'masonry',
                            ),
                        ),
                    ),
                    'freetile' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/freetile',
                            'defaults' => array(
                                'controller' => 'chadisouser',
                                'action'     => 'freetile',
                            ),
                        ),
                    ),

                ),
            ),
        )

    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'ChadisoUser' => __DIR__ . '/../view',
        ),
    ),

    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type'     => 'phparray', //'gettext',
                'base_dir' => __DIR__ . '/../language/phparray',
                'pattern'  => '%s.array.php',//'%s.mo',
                'text_domain' => 'default',
            ),
        ),
    )
);
