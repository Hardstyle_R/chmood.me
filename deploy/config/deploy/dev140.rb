set :application, "chmood.me - development"
set :repository, "roman@192.168.1.140:/home/roman/Projects/chmood.me/gitrepo/chmoodme-dev.git"
#set :repository, "https://Hardstyle_R@bitbucket.org/Hardstyle_R/chmood.me.git"

set :scm, :git

role :web, "192.168.1.140"  # Your HTTP server, Apache/etc
#role :app, ""  # This may be the same as your `Web` server
#role :db,  "your primary db-server here", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

set :user, "deploy"
set :runner, "deploy"
set :password, "hardstyler"
#set :runner, "roman"
set :deploy_to, "/var/www/chmood.me"
set :server_root, "/var/www/chmood.me/current"
#set :copy_exclude, [".git"]


set :keep_releases, 5

set :via, "scp"
set :branch, "master"
#set :branch, fetch(:branch, "specific_branch")
#set :deploy_via, :remote_cache
set :deploy_via, :copy

set :use_sudo, false
set :ssh_options, {:forward_agent => true}
set :port, 22

namespace :deploy do

    task :update do
        transaction do
            update_code
            symlink
        end
    end

    task :finalize_update do
        transaction do
            #run "sudo /etc/init.d/nginx restart"
            #run "chmod -R g+w #{releases_path}/#{release_name}"
        end
    end

    task :symlink do
        transaction do

            run "ln -nfs #{current_release} #{current_path}"

            # logs link
            run "ln -s #{shared_path}/log #{current_path}/logs"

        end
    end

    task :migrate do
        # nothing
    end

    task :restart do
        # nothing
    end

end