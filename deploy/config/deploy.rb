require 'capistrano/ext/multistage'
require 'capistrano/confirm'

set :stages, %w(dev140)
set :default_stage, "dev140"

set :confirm_stages, [:dev140]

#set :branch, fetch(:branch, "specific_branch")