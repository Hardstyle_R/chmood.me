<?php

ini_set('display_errors', 'on');
error_reporting(E_ALL);

//echo phpinfo();
//echo Locale::getDefault();
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

Zend\Debug\Debug::dump('Memory used: ' . (memory_get_usage()/1024/1024) . ' MB');